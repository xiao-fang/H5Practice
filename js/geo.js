// Edit by Edward, Xiao
/*get current geo location via google api.*/

function getGeoLocation() {
    console.info("invoke getGeoLocation.");
    var geoSpan = document.getElementById("geo-location");

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            //on success
            function (pos) {
                var lat = pos.coords.latitude;
                var lon = pos.coords.longitude;

                console.info("current geo location: " + "lat-" + lat + " ; lon-" + lon);
                geoSpan.innerText = "latitude:" + lat + ", longitude:" + lon;
            },

            // on error
            function (err) {
                geoSpan.innerText = "get location error: code=" + err.code + ", msg=" + err.message;
                geoSpan.style.color = "red";
            },

            {
                enableHighAcuracy: true,
                timeout: 5000,
                maximumAge: 3000
            });
    } else {
        //not supported
        geoSpan.innerText = "Your browser does not support GeoLocation";
        geoSpan.style.color = "red";
    }
}
