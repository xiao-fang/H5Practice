function listenToMediaEvents(elementId) {
    var media = document.getElementById(elementId);
    if (media) {

        var storageKey = elementId + '.position';

        function savePos() {
            var pos = media.currentTime;
            if (pos === media.duration) {
                pos = 0;
            }
            saveMediaPosition(storageKey, pos);
        }

        function restorePos() {
            var pos = getMediaPosition(storageKey);
            media.currentTime = pos;
        }

        media.addEventListener("pause", savePos);
        media.addEventListener("ended", savePos);
        media.addEventListener("abort", savePos);
        media.addEventListener("loadedmetadata", restorePos);
    }
}

function saveMediaPosition(storageKey, pos) {
    if (!supportLocalStorage()) {
        return false;
    }
    localStorage[storageKey] = pos;
    return true;
}

function getMediaPosition(storageKey) {
    if (!supportLocalStorage()) {
        return 0;
    }
    if (localStorage[storageKey]) {
        return localStorage[storageKey];
    }
    return 0;
}

function supportLocalStorage() {
    if (window.localStorage) {
        return true;
    } else {
        console.log('This browser does NOT support localStorage');
        return false;
    }
}