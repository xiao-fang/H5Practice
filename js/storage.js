/**
 * Created by xiaofan on 7/25/2015.
 */

/*user model*/
function User(name, pwd, mobile, gender, birth, email, dept, addr) {
    this.UserName = name;
    this.Password = pwd;
    this.Mobile = mobile;
    this.Gender = gender;
    this.Birthday = birth;
    this.Email = email;
    this.Department = dept;
    this.Address = addr;

    /*    this.IsValid = function () {
     return true;
     }*/
}

/*db constants*/
const DB_NAME = 'H5PracticeDB';
const DB_VERSION = '1.0';
const DB_SIZE = 2 * 1024 * 1024;

/*open db with table created*/
function openDB() {
    var db = openDatabase(DB_NAME, DB_VERSION, DB_NAME, DB_SIZE);
    db.transaction(function (trans) {
        trans.executeSql('CREATE TABLE IF NOT EXISTS USERS (UserName TEXT PRIMARY KEY,' +
            'Password TEXT NOT NULL, Mobile INTEGER NOT NULL, Gender TEXT NOT NULL, Birthday TEXT,' +
            'Email TEXT, Department TEXT, Address TEXT)');
    }, function (err) {
        console.error('create db error: ' + err.message);
    });
    return db;
}

/*save user info to db*/
function saveUser(user) {
    if (!user)
        return false;
    openDB().transaction(function (trans) {
        trans.executeSql('DELETE FROM USERS WHERE UserName=?', [user.UserName]);
        trans.executeSql('INSERT INTO USERS VALUES(?,?,?,?,?,?,?,?)',
            [user.UserName, user.Password, user.Mobile, user.Gender,
                user.Birthday, user.Email, user.Department, user.Address]);
    }, function (err) {
        console.error('insert db error: ' + err.message);
        return false;
    }, function () {
        return true;
    });
}

/*get a user from db with username*/
function getUser(userName, resultCallback) {
    openDB().transaction(function (trans) {
        trans.executeSql('SELECT * FROM USERS WHERE UserName=? LIMIT 1', [userName], function (trans, result) {
            if (result.rows.length > 0) {
                //return result.rows[0];
                invokeCallback(result.rows[0]);
            } else {
                invokeCallback(null);
            }
        });
    }, function (err) {
        console.error('get user by name from db error: ' + err.message);
        invokeCallback(null);
    });

    function invokeCallback(result) {
        if (typeof(resultCallback) == 'function') {
            resultCallback(result);
        }
    }
}

/**
 @param {String} userName
 @param {String} [pwd]
 @return {code-msg: 0-suceess, 1-user/pwd empty, 2-user not exist, 3-user/pwd mismatch}
 */
function login(userName, pwd, resultCallback) {

    function invokeCallback(obj) {
        resultCallback(obj);
    }

    var result = {code: 0, msg: "login successfully."};

    if (!userName || !pwd) {
        result.code = 1;
        result.msg = "user name or password cannot be empty.";
        invokeCallback(result);
        return;
    }

    getUser(userName, function (user) {
        if (user) {
            if (user.Password == pwd) {
                result.code = 0;
                result.msg = "login successfully.";
            } else {
                result.code = 3;
                result.msg = "username and password are not match.";
            }
        } else {
            result.code = 2;
            result.msg = "user does not exist, please sign up.";
        }
        invokeCallback(result);
    });
}